const {user_game,user_game_biodata,user_game_histories} = require('../models')

const createUser = async (userInfo) =>{

    const {name,age,gender,email,password} = userInfo

    const user = await user_game.create ({name,email,password})
    
    await user_game_biodata.create ({user_game_id:user.id,age,gender})
    return user
}

const userLogin = async (loginInfo) =>{
    
    const {email,password} = loginInfo
    const user = await user_game.findOne({
        where: {
            email
        }
    })

    if(!user){
        console.log('email tidak terdaftar')
        return null
    }
    
    if(user.password !== password){
        console.log('pasword salah')
        return null        
    }
    
    return user
}

const getAllUser = async() =>{
    
    const users = await user_game.findAll({
        include: [user_game_biodata,user_game_histories]
    })
     return users   
}

module.exports = {createUser,userLogin,getAllUser}

