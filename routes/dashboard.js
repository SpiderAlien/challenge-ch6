const { response } = require('express')
const express = require('express')
const { DATE } = require('sequelize')
const router = express.Router()
const users = []
const {user_game,user_game_biodata,user_game_histories} = require('../models')
const {createUser, getAllUser} = require('../services/user')

router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

router.get('/', (req,res,) => {
    res.render('dashboard')
})


router.get('/dashboard', async(req,res,) => {
    const users = await getAllUser()
    res.render('userAdd',{
        users
    })
})

router.get('/userAdd', (req, res) =>{
    res.render('/userAdd')
})

router.post('/userAdd', async (req, res) =>{

    const user = await createUser(req.body)
    users.push(user)
    // console.log(users)
    res.redirect('/dashboard')

})


module.exports = router
