const { req } = require("express")
const express = require ("express")
const router = express.Router ()
const users = []

router.use((req, res, next) => {
    console.log("route level middleware")
    next()
})

router.get("/home", (req, res) =>{
    res.render("home")
})

router.get("/game", (req, res) =>{
    res.render("game")
})

router.get("/dashboard", (req, res) =>{
    res.render("dashboard")
})

router.get("/userAdd", (req, res) =>{
    res.render("userAdd")
})

module.exports = router