const { req, res } = require("express")
const express = require ("express")
const router = express.Router ()
const users = []
const {user_game,user_game_biodata,user_game_histories} = require('../models')
const {createUser, userLogin} = require('../services/user')

router.use((req, res, next) => {
    console.log("route level middleware")
    next()
})

router.get("/login", (req, res) =>{
    res.render("login")
})
router.post('/login', async (req,res) => {
    
    const user = await userLogin(req.body)   

    if(!user){
        return res.render('error')
    }


    // const user_data = await user_game.findOne({
    //     where: {
    //         email
    //     }, 
    //     include: [user_game_biodata,user_game_histories]
    // })
    // res.json(user_data)

    users.push({email,password})
    console.log(users)
    res.render('/')    
})

router.get('/register', (req, res) =>{
    res.render('register')
})

router.post('/register', async (req, res) =>{

    const user = await createUser(req.body)
    users.push(user)
    // console.log(users)
    res.redirect('/auth/login')

})


module.exports = router